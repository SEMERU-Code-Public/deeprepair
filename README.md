# DeepRepair

`out/trials` contains a directory for each project in [Defects4J version 1.1.0](https://github.com/rjust/defects4j).
- Each `out/trials/<project>` contains a directory for every program revision, e.g., `out/trials/Chart/1/b` contains all the results for Chart-1b, the buggy program revision for Chart-1.
- The directory for each buggy program revision contains 18 subdirectories where each subdirectory corresponds to one configuration.
- Each configuration will contain numbered subdirectories corresponding to different random seeds. For example, `out/trials/Chart/1/b/random-global-default/1/pbs.log` is Chart-1b's log file for the baseline configuration (at global-level scope) using `seed=1`.